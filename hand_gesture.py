import cv2
import mediapipe as mp 

video_cap = cv2.VideoCapture("./hand_gesture.mp4")##replace with 0 for live hand gesture tracking

mpHands = mp.solutions.hands

hands = mpHands.Hands()

mpdraw = mp.solutions.drawing_utils

while video_cap.isOpened():
    
    ret , frame = video_cap.read()

    frame = cv2.resize(frame , (480 , 540))
    
    image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    
    results = hands.process(image)
    
    if results.multi_hand_landmarks:
        for handlms in results.multi_hand_landmarks:
            mpdraw.draw_landmarks(frame , handlms , mpHands.HAND_CONNECTIONS)
            
    cv2.imshow('image' , frame)
    
    if cv2.waitKey(1) != -1:
        break
        
video_cap.release()
cv2.destroyAllWindows()