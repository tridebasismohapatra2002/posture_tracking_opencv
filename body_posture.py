import cv2
import mediapipe as mp 

mp_holistic = mp.solutions.holistic
holistic = mp_holistic.Holistic()
mp_drawing = mp.solutions.drawing_utils

cap = cv2.VideoCapture("./body_posture.mp4")##replace with 0 for live body gesture tracking

while cap.isOpened():
    ret, frame = cap.read()
    if not ret:
        break
        
    frame = cv2.resize(frame , (480 , 540))

    frame_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    
    results = holistic.process(frame_rgb)

    '''if results.face_landmarks:
        mp_drawing.draw_landmarks(frame, results.face_landmarks, mp_holistic.FACEMESH_CONTOURS)
    '''
    if results.pose_landmarks:
        mp_drawing.draw_landmarks(frame, results.pose_landmarks, mp_holistic.POSE_CONNECTIONS)
        
    '''if results.left_hand_landmarks:
        mp_drawing.draw_landmarks(frame, results.left_hand_landmarks, mp_holistic.HAND_CONNECTIONS)
    '''
    '''if results.right_hand_landmarks:
        mp_drawing.draw_landmarks(frame, results.right_hand_landmarks, mp_holistic.HAND_CONNECTIONS)'''
    
    cv2.imshow('Body Gesture Tracking', frame)

    if cv2.waitKey(1) != -1:
        break

cap.release()
cv2.destroyAllWindows()